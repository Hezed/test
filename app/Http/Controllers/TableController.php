<?php

namespace App\Http\Controllers;
use App\Models\Table;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TableController extends Controller
{
    public function Index(){
        $tables = Table::all();

        return view('table',['tables' => $tables]);
    }
    public function myIndex(){
        // $mytable = User::find(1)->myTable;
        // $mytables = Table::all($id);
        
        return view('myTable',['mytables' => $mytables]);
    }
    // $mytables =Auth::id()=$mytables->user_id;
    // return view('table-update',['tables'=>$tables->find($id)]);
    // ->auth()->check()
    // ->find($id)
    // Auth::id()
    public function delete($id){
        Table::where('id',$id)->delete();

        return redirect()->route('table')->with('success','Обзор Успешно удален');
    }

    public function addTable(){
        return view('table-form');

    }

    public function submit(Request $req ){
        $validation = $req->validate([
            'header' => 'required|min:15|max:150',
            'body' => 'required|min:20'
        ]);
        $submit = new Table;
        $submit->header = $req->input('header');
        $submit->body = $req->input('body');
        $submit->user_id = auth()->id();
        $submit->save();

        return redirect()->route('table')->with('success','Успешно');
    }

    public function update($id){
        $tables = new Table;

        return view('table-update',['tables'=>$tables->find($id)]);
    }

    public function updateSubmit($id ,Request $req){
        // $validation = $req->validate([
        //     'header' => 'required|min:15|max:150',
        //     'body' => 'required|min:20'
        // ]);
        $submit = Table::find($id);
        $submit->header = $req->input('header');
        $submit->body = $req->input('body');

        $submit->save();

        return redirect()->route('table');
    }

//     // public function home(){
//     //     $table = new Table;
//     //     return view('home',['data'=> [$table->inRandomOrder()->first()]]);

//     // }
// }
}
