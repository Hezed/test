<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class loginController extends Controller
{
    public function login(){
        return view('auth.login');

    }
    public function submit(request $req){


        // dd($req);
        $validation = $req->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if(auth()->attempt(['email' => $req->email, 'password'=> $req->password])){
            return redirect()->home();
        }
             return redirect()->back();
    }
    public function logout(){
        auth()->logout();
        return redirect()->back();
        // return redirect()->home();

    }

}
