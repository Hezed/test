<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class registerController extends Controller
{

    
    
    public function register(request $req){
        $validation = $req->validate([
            'name' => 'required|max:20',
            'email' => 'required|email',
            'password' => 'required|min:5|max:25'

    ]);
        $user = new User;
        $user->name=$req->name;
        $user->email=$req->email;
        $user->password=Hash::Make($req->password);
        $user->save();

        auth()->login($user);

        return redirect()->home();
    }
}
