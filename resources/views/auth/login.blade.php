@extends('layout.app')
@section('content')
<br><br><br>
<div class="container">
<div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
<h1>Вход в Учётную Запись.</h1>
<hr>
    <div class="col-md-6 px-0"></div>
<form action="{{ route('login-submit')}}"method="post">
@csrf
    <div class="dorm-group">
        <label for="email"></label>
        <input type="varchar" name="email" placeholder="Введите Email" id="email" class="form-control">
    </div>
    <br>
    <div class="dorm-group">
        <label for="password"></label>
        <input type="varchar" name="password" placeholder="Введите Пароль" id="password" class="form-control">
    </div>
    <br>
<hr>
<button type="sumit" class="btn btn-success btn-lg">Войти</button>
</form>

@endsection('content')