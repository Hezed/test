<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body class="text-center bg-light ght"><nav class="navbar navbar-expand-md navbar-dark bg-warning fixed-top">
<button type="button" class="btn btn-outline-primary btn-lg">MEgaCOrp</button>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
    
    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
    <a class="nav-link active" href="{{route('home')}}">
    <button type="button" class="btn  btn-outline-primary" >Главная</button></a>
      <a class="nav-link active" href="{{route('table')}}">
      <button type="button" class="btn btn-outline-primary" href="{{route('table')}}" >Список статей</button></a>
        <a class="nav-link active" href="{{route('table')}}">
        <button type="button" class="btn btn-outline-primary" >Пока хз</button></a>
</div>

    </ul>
  </div>
  @if(auth()->check())
  <a class="nav-link active" href="{{route('myTable')}}">
  <button type="button" class="btn btn-success">Мои Статьи</button></a>
  <a class="nav-link" href="{{route('logout')}}"><button type="button" class="btn btn-danger">Выйти</button></a>
  @else
  <a class="nav-link" href="{{route('login')}}"><button type="button" class="btn btn-success">Вход</button></a>
  <a class="nav-link" href="{{route('register')}}"><button type="button" class="btn btn-success">Регистрация</button></a>
  @endif      



</nav>

    <div class="container">
    <br><br><br>
        @include('layout.message')
        @yield('content')
    </div>
</body>
</html>