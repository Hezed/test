@extends('layout.app')
@section('content')
<table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Заголовок</th>
      <th scope="col">Обзор</th>
      <th scope="col">Дата</th>
      <th scope="col">Взаимодействия:

      <a class="btn btn-success btn-sm" href="{{route('table-form')}}">Добавить Статью</a>
      </th>
    </tr>
  </thead>
  <tbody>
  @foreach($tables as $table)
    <tr>
      <th scope="row">{{$table->id}}</th>
      <td>{{$table->header}}</td>
      <td>{{$table->body}}</td>
      <td>Нет</td> 
      <!-- <td>
        <a class="btn btn-danger btn-sm" href="{{route('deleteTable',$table->id)}}">Удалить</a>
        <a class="btn btn-warning btn-sm" href="{{route('table-update',$table->id)}}" >Редактировать</a> 
      </td> -->
    </tr>
@endforeach
</tbody>
</table>
@endsection('content')