@extends('layout.app')
@section('content')
<table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Заголовок</th>
      <th scope="col">Обзор</th>
      <th scope="col">Дата</th>
      <th scope="col">Взаимодействия:

      <a class="btn btn-success btn-sm" href="{{route('table-form')}}">Добавить Статью</a>
      </th>
    </tr>
  </thead>
  <tbody>
  @foreach($mytables as $mytable)
    <tr>
      <th scope="row">{{$mytable->user_id}}</th>
      <td>{{$mytable->header}}</td>
      <td>{{$mytable->body}}</td>
      <td>Нет</td> 
      <td>
        <a class="btn btn-danger btn-sm" href="{{route('deleteTable',$mytable->id)}}">Удалить</a>
        <a class="btn btn-warning btn-sm" href="{{route('table-update',$mytable->id)}}" >Редактировать</a> 
      </td>
    </tr>
@endforeach
</tbody>
</table>
@endsection('content')