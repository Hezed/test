<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\loginController;
use App\Http\Controllers\registerController;
use App\Http\Controllers\TableController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('home');})->name('home');

Route::get('/login', [loginController::class, 'login'])->name('login');
Route::post('/login', [loginController::class, 'submit'])->name('login-submit');
Route::get('/login/logout', [loginController::class, 'logout'])->name('logout');

Route::get('/register', function () {return view('auth.register');})->name('register');
Route::post('/register/submit',[registerController::class, 'register'])->name('register-submit');

Route::get('/table', [TableController::class, 'index'])->name('table');
Route::get('/myTable', [TableController::class, 'myIndex'])->name('myTable');
Route::get('/table/{id}',[TableController::class, 'Delete'])->name('deleteTable');
Route::get('/table/form', [TableController::class,'addTable'])->name('table-form');
Route::post('/table/form/submit',[TableController::class,'submit'])->name('table-submit');
Route::get('/table/update/{id}',[TableController::class,'update'])->name('table-update');
Route::post('/table/update/{id}',[TableController::class,'updateSubmit'])->name('update-submit');

